import Vue from 'vue'
import Router from 'vue-router'
import Site from '@/components/Site'
import Dashboard from '@/components/Dashboard'
import AdminPanel from '@/components/AdminPanel'
import ViewProduct from '@/components/ViewProduct'
import EditProduct from '@/components/EditProduct'
import NewProduct from '@/components/NewProduct'
import Login from '@/components/Login';
import Register from '@/components/Register';
import firebase from 'firebase';

Vue.use(Router)

let router = new Router({
  routes: [
    {
      path: '/',
      name: 'site',
      component: Site,
      meta: {
        requiresAuth: false
      }
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: Dashboard,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/adminpanel',
      name: 'adminpanel',
      component: AdminPanel,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
      meta: {
        requiresGuest: true
      }
    },
    {
      path: '/register',
      name: 'register',
      component: Register,
      meta: {
        requiresGuest: true
      }
    },
    {
      path: '/new',
      name: 'new-product',
      component: NewProduct,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/:product_id',
      name: 'view-product',
      component: ViewProduct,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/edit/:product_id',
      name: 'edit-product',
      component: EditProduct,
      meta: {
        requiresAuth: true
      }
    }
  ]
})

router.beforeEach((to, from, next) => {
  // Check for requiresAuth guard
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // Check if NO logged user
    if (!firebase.auth().currentUser) {
      // Go to login
      next({
        path: '/login',
        query: {
          redirect: to.fullPath
        }
      });
    } else {
      // Proceed to route
      next();
    }
  } else if (to.matched.some(record => record.meta.requiresGuest)) {
    // Check if NO logged user
    if (firebase.auth().currentUser) {
      // Go to login
      next({
        path: '/adminpanel',
        query: {
          redirect: to.fullPath
        }
      });
    } else {
      // Proceed to route
      next();
    }
  } else {
    // Proceed to route
    next();
  }
});

export default router;
